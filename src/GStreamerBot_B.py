#!/usr/bin/env python
# -*-coding: utf-8 -*-

from os import *
from os.path import isfile, join, isdir
import sys

version = "Primo tentativo Gstreamer interactor"
var = ""
newFileNameExtension = ""
lastNumberOfFile = 0
found = False
greatestNumber = False
if len(sys.argv) == 2:
    var = sys.argv[1]
elif len(sys.argv) == 3:
    if sys.argv[2].isdigit():
        var = sys.argv[1]
        lastNumberOfFile = int(sys.argv[2])
if var == "":
    var = "noName"
isdir = isdir("/mnt/buffer/B_Cam/")
if isdir:
    dirs = listdir("/mnt/buffer/B_Cam/")
    onlyfiles = ''
    for file in dirs:
        if isfile(join("/mnt/buffer/B_Cam/", file)):
            indexExtension = file.rfind(".")
            fileNameNoExtension = file[:indexExtension]
            indexUnderscore = fileNameNoExtension.rfind("_")
            if indexUnderscore > -1:
                fileNameNoUnderscore = fileNameNoExtension[:indexUnderscore]
                if var == fileNameNoUnderscore:
                    numberOfFile = fileNameNoExtension[indexUnderscore + 1:]
                    if numberOfFile.isdigit():
                        if int(numberOfFile) >= lastNumberOfFile:
                            lastNumberOfFile = int(numberOfFile)
                            greatestNumber = True
                    found = True
            if var == fileNameNoExtension:
                found = True
    if found:
        if greatestNumber:
            lastNumberOfFile = lastNumberOfFile + 1
        underscorePlusNumber = ""
        if lastNumberOfFile > 9:
            underscorePlusNumber = join("_", str(lastNumberOfFile))
        elif lastNumberOfFile < 10:
            underscorePlusNumber = join("_", "0" + str(lastNumberOfFile))
        newFileName = join(var, underscorePlusNumber)
        newFileNameExtension = join(newFileName, ".mp4")
elif not isdir:
    print("Non è una directory valida")
    sys.exit(0)
elif var == "Esci":
    sys.exit(0)
if not found:
    if len(sys.argv) == 3:
        if lastNumberOfFile > 9:
            underscorePlusNumber = join("_", str(lastNumberOfFile))
        elif lastNumberOfFile < 10:
            underscorePlusNumber = join("_", "0" + str(lastNumberOfFile))
    elif len(sys.argv) < 3:
        underscorePlusNumber = join("_", "01")
    newFileName = join(var, underscorePlusNumber)
    newFileNameExtension = join(newFileName, ".mp4")
newFileNameExtension = newFileNameExtension.replace("/", "")

command = "gst-launch-1.0 -v v4l2src device=/dev/video0 ! videoscale ! 'video/x-raw,width=1280, height=720, " \
          "framerate=25/1, format=YUY2' ! nvvidconv ! omxh264enc control-rate=2 bitrate=6000000 ! queue ! mux. " \
          "alsasrc ! audio/x-raw,width=16,depth=16,rate=32000,channel=1 ! queue ! audioconvert ! audioresample ! " \
          "voaacenc ! aacparse ! qtmux name=mux ! filesink location=/mnt/buffer/B_Cam/" + newFileNameExtension + "" \
           " sync=false -e "

system(command)
