import sys

from easygui import *
from os import listdir
from os.path import isfile, join, isdir

version = "Primo tentativo Gstreamer interactor"

# var = buttonbox(title=version, choices=["Scegli la directory", "Esci"])
# fileName = enterbox(msg="Inserisci il nome del file")

var = filesavebox(msg="File da salvare", title=version)

# if var == "Scegli la directory":
#     path = diropenbox()
#     msgbox(path, title=version)
#     sys.exit(0)
# elif var == "Esci":
#     sys.exit(0)

if var != "":
    index = var.rfind("\\")
    fileDirectory = var[:index]
    print(fileDirectory)
    fileName = var[index + 1:]
    print(fileName)
    isdir = isdir(fileDirectory)
    found = False
    if isdir:
        dirs = listdir(fileDirectory)
        onlyfiles = ''
        for file in dirs:
            if isfile(join(fileDirectory, file)):
                # file = join(file, '\n')
                # onlyfiles = join(onlyfiles, file)
                # onlyfiles = onlyfiles.replace('\\', '')
                indexExtension = file.rfind(".")
                fileNameNoExtension = file[:index]
                indexUnderscore = fileNameNoExtension.rfind("_")
                if indexUnderscore > -1:
                    fileNameNoUnderscore = fileNameNoExtension[:indexUnderscore]
                    if file == fileNameNoUnderscore:
                        numberOfFile = fileNameNoExtension[indexUnderscore + 1:]
                        if isinstance(numberOfFile, int):
                            underscorePlusNumber = join("_", numberOfFile + 1)
                            newFileName = join(fileName, underscorePlusNumber)
                            newFileNameExtension = join(fileName, ".mp4")
                        elif not isinstance(numberOfFile, int):
                            underscorePlusNumber = join("_", "1")
                            newFileName = join(fileName, underscorePlusNumber)
                            newFileNameExtension = join(fileName, ".mp4")
                        found = True
                        msgbox(file, title=version)
                        break
                if file == fileNameNoExtension:
                    found = True
                    underscorePlusNumber = join("_", "1")
                    newFileName = join(fileName, underscorePlusNumber)
                    newFileNameExtension = join(fileName, ".mp4")
                    msgbox(newFileNameExtension, title=version)
                    break
        # msgbox(onlyfiles, title=version)
    elif not isdir:
        msgbox("Directory doesn't exist", title=version)
        sys.exit(0)
elif var == "Esci":
    sys.exit(0)
